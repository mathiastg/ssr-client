export type CallbackHandler = (
  request: Request,
  params: Record<string, string>,
) => Promise<Response> | Response;

interface Handler {
  pattern: URLPattern;
  callback: CallbackHandler;
}
export class Router {
  routes: Record<string, Array<Handler>> = {
    "GET": [],
    "POST": [],
    "PUT": [],
  };
  add(method: string, pathname: string, handler: CallbackHandler) {
    this.routes[method].push({
      pattern: new URLPattern({ pathname }),
      callback: handler,
    });
  }
  route(req: Request): Promise<Response> | Response {
    for (const route of this.routes[req.method]) {
      if (route.pattern.test(req.url)) {
        const executedRoute = route.pattern.exec(req.url);
        if (executedRoute) {
          const params = executedRoute.pathname.groups;
          return route.callback(req, params);
        }
      }
      console.log(`${route.pattern.pathname} doesnt match ${req.url}`);
      console.log(`${this.routes["GET"]}`);
    }
    return new Response(null, { status: 404 });
  }
}
