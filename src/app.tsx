import { React } from "../deps.ts";
import Home from "./home/home.tsx";

const App = () => {
  return (
    <html>
      <head>
        <link rel="stylesheet" href="/public/style.css" />
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        </meta>
        <link
          rel="icon"
          type="image/x-icon"
          href="/public/assets/icons/grid.svg"
        >
        </link>
        <title>Gleerup's Playground</title>
      </head>
      <body>
        <Home />
      </body>
    </html>
  );
};

export default App;
