import { React } from "/deps.ts";
import Icon from "/src/util/icon.tsx";
const topBarStyle: React.CSSProperties = {
  // backgroundColor: darken(colors.$primary, 5%);
  backgroundColor: "#e64d1f",
  width: "100%",
  height: "3rem",
  display: "flex",
  flexDirection: "row",
};

const icon: React.CSSProperties = {
  width: "3rem",
  height: "3rem",
  alignSelf: "flex-end",
};

const TopBar = () => (
  <div style={topBarStyle}>
    <div className="filler"></div>
    <div style={icon}>
      <Icon namespace="settings" name="toggleswitch"></Icon>
    </div>
  </div>
);

export default TopBar;
