import { React } from "/deps.ts";

import TopBar from "./top-bar.tsx";

const mystyle: React.CSSProperties = {
  display: "flex",
  overflowY: "hidden",
  overflowX: "hidden",
  flex: "1 1",
  width: "100%",
  height: "100%",
};
const Home = () => (
  <div style={mystyle}>
    <div className="filler">
      <TopBar />
    </div>
  </div>
);
export default Home;
