import { React } from "../../deps.ts";

const iconStyle: React.CSSProperties = {
  width: "100%",
  height: "100%",
  lineHeight: "0",
};

interface IconDef {
  namespace: string;
  name: string;
}

const Icon: React.FunctionComponent<IconDef> = (
  { namespace, name },
) => (
  <img
    style={iconStyle}
    src={`/public/assets/icons/${namespace}/${name}.svg`}
  >
  </img>
);
export default Icon;
