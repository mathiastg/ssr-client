import { React, ReactDOMServer } from "./deps.ts";
import { serveFile } from "./public-files.ts";
import { Router } from "./router.ts";
import App from "/src/app.tsx";

const server = Deno.listen({ port: 8000, transport: "tcp" });
console.log(`HTTP webserver running.  Access it at:  http://localhost:8000/`);

const router = new Router();
router.add("GET", "/", page);
router.add("GET", "/public/*", files);

function page(
  request: Request,
  params: Record<string, string>,
): Promise<Response> | Response {
  const body = ReactDOMServer.renderToString(<App />);
  return new Response(body, {
    status: 200,
    headers: {
      "content-type": "text/html; charset=utf-8",
    },
  });
}

function files(
  request: Request,
  params: Record<string, string>,
): Promise<Response> {
  return serveFile(params["0"]);
}

// Connections to the server will be yielded up as an async iterable.
for await (const conn of server) {
  // In order to not be blocking, we need to handle each connection individually
  // without awaiting the function
  serveHttp(conn);
}
async function serveHttp(conn: Deno.Conn) {
  // This "upgrades" a network connection into an HTTP connection.
  const httpConn = Deno.serveHttp(conn);
  // Each request sent over the HTTP connection will be yielded as an async
  // iterator from the HTTP connection.
  for await (const requestEvent of httpConn) {
    const response = router.route(requestEvent.request);
    requestEvent.respondWith(response);
  }
}

function isRoot(path: string): boolean {
  return path === "/" || path === "" || path === "/index.html";
}
