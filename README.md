# A test implementation of website in Deno using Ultra.

## Tasks

- [ ] Why is --allow-env needed? ultra reads HOME env var but theres no apparent
      reason why....
- [ ] port the rest of the functionality of angular client. Mostly. We don't
      want all sort of weird things like unneccesary subjects etc., just the
      basic functionality
- [ ] test if icons change dynamically or if we need to do something with
      useState
- [ ] implement menu from slack conversation.
- [ ] implement a overlay where the menu appears.
- [ ] implement a listener for TouchMove events that evaluates a circle
- [ ] make the overlay appear when the circle is asserted. Maybe change to a
      double circle within the same TouchStarted/Down/ event
- [ ] make the overlay appear at the center of the drawn circle
- [ ] make the menu be dynamic to number of menu entries
