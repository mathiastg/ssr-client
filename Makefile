config = --allow-net --allow-read --import-map importmap.json

dev:
	mode=dev deno run $(config) server.tsx

start:
	deno run $(config) server.tsx

build:
	deno compile $(config) server.tsx

cache:
	deno cache --import-map=importmap.json --reload --no-check server.tsx
